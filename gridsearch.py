import os
from tqdm import tqdm
import pandas as pd
import networkx as nx
from itertools import product
from energeai.interpolation import feature_propagation

from src.models import *
from src.benchmarks import benchmarks


@torch.no_grad()
def zero_interpolation(
        data: pd.DataFrame,
        topology: nx.Graph,
        sensors: list[int]
) -> pd.DataFrame:
    x = torch.tensor(data.values, dtype=torch.cfloat)
    out = torch.zeros_like(x)
    out[:, sensors] = x[:, sensors]
    return pd.DataFrame(out.numpy(), dtype=complex,
                        index=data.index, columns=data.columns)


path = os.path.join(os.path.dirname(__file__), "results.csv")
columns = ("model", "layers", "fp", "adm", "#params", "train_base_mse",
           "train_base_res", "test_base_mse", "test_base_res", "od_mse",
           "od_res", "tc1_mse", "tc1_res", "tc2_mse", "tc2_res", "pq2mv_mse",
           "pq2mv_res", "mv2pq_mse", "mv2pq_res")
models = (GCN, GAT, GIN, GraphSAGE)
layers = range(1, 11)
bits = [True, False]

try:
    results = pd.read_csv(path)
except FileNotFoundError:
    results = pd.DataFrame(columns=columns)
    results.to_csv(path, index=False, header=True)

for params in tqdm(product(layers, models, bits, bits),
                   total=4*len(models)*len(layers)):
    nl, model, fp, adm = params
    existing_row = results[(results['model'] == model.__name__) &
                           (results['layers'] == nl) &
                           (results['fp'] == fp) & (results['adm'] == adm)]
    if existing_row.empty:
        fun = feature_propagation if fp else zero_interpolation
        new_row = pd.DataFrame([{
            "model": model.__name__,
            "layers": nl,
            "fp": fp,
            "adm": adm,
        } | benchmarks(
            model_class=model,
            num_layers=nl,
            use_admittance=adm,
            interpolation_fn=fun
        )])
        new_row.to_csv(path, mode='a', header=False, index=False)
        results = pd.concat([results, new_row], ignore_index=True)

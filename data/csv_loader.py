import os
import csv
import cmath
from math import radians
import numpy as np
import pandas as pd
import pandapower as pp
from functools import cache


@cache
def load_csv(dataset: str):
    path = os.path.join(os.path.dirname(__file__), dataset)
    net = pp.create_empty_network()

    # Load node input model
    nodes = pd.read_csv(
        os.path.join(path, "node_input.csv"),
        sep=';', quoting=csv.QUOTE_NONE,
        header=0, index_col=0, usecols=[0, 8])
    reverse_lookup = dict()
    for index, row in nodes.iterrows():
        reverse_lookup[index] = pp.create_bus(net, row['v_rated'])

    # Load line input model
    lines = pd.read_csv(
        os.path.join(path, "line_input.csv"),
        sep=';', quoting=csv.QUOTE_NONE, header=0, index_col=0)
    line_type = pd.read_csv(
        os.path.join(path, "line_type_input.csv"),
        sep=';', quoting=csv.QUOTE_NONE, header=0, index_col=0)
    lines = pd.merge(lines, line_type,
                     left_on='type', right_index=True, how='inner')
    for index, row in lines.iterrows():
        node_a = reverse_lookup[row['node_a']]
        node_b = reverse_lookup[row['node_b']]
        pp.create_line_from_parameters(
            net, node_a, node_b, row['length'], row['r'], row['x'], 0,
            row['i_max'], parallel=row['parallel_devices'])

    # Load transformer input model
    trafos = pd.read_csv(
        os.path.join(path, "transformer_2_w_input.csv"),
        sep=';', quoting=csv.QUOTE_NONE, header=0, index_col=0)
    trafo_type = pd.read_csv(
        os.path.join(path, "transformer_2_w_type_input.csv"),
        sep=';', quoting=csv.QUOTE_NONE, header=0, index_col=0)
    trafos = pd.merge(trafos, trafo_type,
                     left_on='type', right_index=True, how='inner')
    for index, row in trafos.iterrows():
        node_a = reverse_lookup[row['node_a']]
        node_b = reverse_lookup[row['node_b']]
        pp.create_transformer_from_parameters(
            net, node_a, node_b, row['s_rated'],
            row['v_rated_a'], row['v_rated_b'], row['r_sc'], row['x_sc'],
            0, 0, parallel=row['parallel_devices'])

    # Load switch input model
    switches = pd.read_csv(
        os.path.join(path, 'switch_input.csv'),
        sep=';', quoting=csv.QUOTE_NONE, header=0, index_col=0)
    switch_reverse_lookup = dict()
    for index, row in switches.iterrows():
        node_a = reverse_lookup[row['node_a']]
        node_b = reverse_lookup[row['node_b']]
        switch_reverse_lookup[index] = pp.create_switch(
            net, node_a, node_b, 'b', closed=row['closed'])

    # Load switch states if any
    if os.path.isfile(os.path.join(path, "switch_res.csv")):
        switch_states = pd.read_csv(
            os.path.join(path, "switch_res.csv"),
            sep=',', quoting=csv.QUOTE_NONE, header=0, index_col=0)
        switch_states = switch_states.pivot(
            index="time", columns="input_model", values="closed"
        ).astype("boolean")
        switch_states = switch_states.rename(columns=switch_reverse_lookup)
    else:
        switch_states = None

    # Load voltages
    if os.path.isfile(os.path.join(path, "std_voltage.h5")):
        std_voltage = pd.read_hdf(os.path.join(path, "std_voltage.h5"),
                                  key="voltage")
        norms = pd.read_hdf(os.path.join(path, "std_voltage.h5"), key="norms")
    else:
        voltage = pd.read_csv(
            os.path.join(path, "node_res.csv"),
            sep=',', quoting=csv.QUOTE_NONE, header=0, index_col=0)
        voltage["v"] = voltage.apply(
            lambda x: cmath.rect(x["v_mag"], radians(x["v_ang"])), axis=1)
        voltage = voltage.drop(["v_mag", "v_ang", "p", "q"], axis=1).pivot(
            index="time", columns="input_model", values="v")
        voltage = voltage.rename(columns=reverse_lookup)
        voltage = voltage.reindex(sorted(voltage.columns), axis=1)

        # Normalize voltages
        real_data = np.real(voltage.values)
        imag_data = np.imag(voltage.values)
        real_data = (real_data - real_data.mean()) / real_data.std()
        imag_data = (imag_data - imag_data.mean()) / imag_data.std()
        std_voltage = pd.DataFrame(real_data + 1j * imag_data,
                                   index=voltage.index,
                                   columns=voltage.columns)

        # Remember the normalization factors to compute voltage bands later
        norms = pd.Series([voltage.values.mean(), voltage.values.std()])

    return net, std_voltage, norms, switch_states


if __name__ == '__main__':
    path = os.path.join(os.path.dirname(__file__))
    net, std_voltage, norms, _ = load_csv("simbench_pq")
    std_voltage.to_hdf(os.path.join(path, "simbench_pq", "std_voltage.h5"),
                       key="voltage", mode='w')
    norms.to_hdf(os.path.join(path, "simbench_pq", "std_voltage.h5"),
                 key="norms", mode='a')
    net, std_voltage, norms, switch_states = load_csv("simbench_tc")
    std_voltage.to_hdf(os.path.join(path, "simbench_tc", "std_voltage.h5"),
                       key="voltage", mode='w')
    norms.to_hdf(os.path.join(path, "simbench_tc", "std_voltage.h5"),
                 key="norms", mode='a')

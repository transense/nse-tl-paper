import os
from cmath import rect
from math import radians
import numpy as np
import pandas as pd
import pandapower as pp


def load_cigre_mv():
    net = pp.networks.create_cigre_network_mv(with_der="all")

    store = pd.HDFStore(os.path.join(os.path.dirname(__file__), "_outputs",
                                     "carl_cigre_ts.hdf5"), mode='r')
    keys = store.keys()
    df = pd.read_hdf(store, key=next(k for k in keys if "Powergrid" in k))
    store.close()

    mapper = lambda col: int(col.split('__')[2])
    buses = set(mapper(col) for col in df.columns if 'bus' in col)
    mag = df[[f'0__bus__{bus}___vm_pu' for bus in buses]]
    mag = mag.rename(mapper, axis='columns')
    phi = df[[f'0__bus__{bus}___va_degree' for bus in buses]].map(radians)
    phi = phi.rename(mapper, axis='columns')
    rect_vec = np.vectorize(rect)
    voltage = pd.DataFrame(rect_vec(mag, phi),
                           index=mag.index, columns=mag.columns)

    real_data = np.real(voltage.values)
    imag_data = np.imag(voltage.values)
    real_data = (real_data - real_data.mean()) / real_data.std()
    imag_data = (imag_data - imag_data.mean()) / imag_data.std()
    std_voltage = pd.DataFrame(real_data + 1j * imag_data, dtype=complex,
                               index=voltage.index, columns=voltage.columns)

    # Remember the normalization factors to compute voltage bands later
    norms = pd.Series([voltage.values.mean(), voltage.values.std()])

    return net, std_voltage, norms


if __name__ == '__main__':
    load_cigre_mv()

from collections import OrderedDict
from collections.abc import MutableSequence
from typing import TypeVar, Union

_T = TypeVar("_T")


class DeduplicatedList(MutableSequence):
    """
    This is a somewhat confusing and poorly implemented collection that I
    do not recommend copying. Its purpose is to efficiently store a list
    that consists of long contiguous ranges of repeating elements.
    """

    def __init__(self):
        self.data = OrderedDict()

    def __getitem__(self, index: Union[int, slice]):
        while index < 0:
            index += len(self)
        for rng, item in self.data.items():
            if index in rng:
                return item
        raise IndexError

    def __setitem__(self, index: int, value: _T) -> None:
        raise NotImplementedError

    def __delitem__(self, index: int) -> None:
        raise NotImplementedError

    def __iter__(self):
        for rng, item in self.data.items():
            for _ in rng:
                yield item

    def __len__(self) -> int:
        if self.data:
            return next(reversed(self.data.keys())).stop
        else:
            return 0

    def insert(self, index: int, value: _T) -> None:
        """
        Pardon the confusion, but this method actually _appends_ `index`
        elements `value` to the collection. This one just has a more
        suitable signature than `append`.
        """
        last = len(self)
        self.data[range(last, last + index)] = value

    def extend(self, length: int) -> None:
        """
        There are no brakes on the confusion train!
        This method only _extends_ the last entry of the list by the given
        length.
        """
        if not len(self):
            raise IndexError
        oldrng = next(reversed(self.data.keys()))
        newrng = range(oldrng.start, oldrng.stop + length)
        self.data[newrng] = self.data.pop(oldrng)

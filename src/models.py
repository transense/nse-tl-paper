from abc import ABC
from typing import Iterable
import numpy as np
import torch
import torch_geometric as pyg


class GNN(torch.nn.Module, ABC):
    def __init__(self, num_layers: int = 1, use_admittance: bool = True):
        super().__init__()
        self.a = use_admittance
        self.num_layers = num_layers
        self.num_features = 2

    def forward(self, x, edge_index, edge_attr):
        edge_attr = edge_attr if self.a else None
        x = self.layer(x, edge_index, edge_weight=edge_attr).squeeze()
        pred = torch.view_as_complex(x)
        return pred

    def fit(self, dataset: Iterable[pyg.data.Data]):
        loss2plot = []
        optimizer = torch.optim.Adam(self.parameters())
        for data in dataset:
            y_pred = self.forward(data.x, data.edge_index, data.edge_attr)
            y_true = torch.view_as_complex(data.y)
            loss = torch.mean(abs(y_true - y_pred) ** 2)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()
            loss2plot.append(loss.cpu().item())
        return loss2plot

    def count_params(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)

    @torch.no_grad()
    def predict(self, dataset: Iterable[pyg.data.Data]):
        y_pred = np.array([
            self.forward(data.x, data.edge_index, data.edge_attr).cpu()
            for data in dataset])
        y_true = np.array([torch.view_as_complex(s.y).cpu() for s in dataset])
        return y_pred, y_true


class GCN(GNN):
    def __init__(self, num_layers: int = 1, use_admittance: bool = True):
        super().__init__(num_layers, use_admittance)
        self.layer = pyg.nn.models.GCN(
            in_channels=self.num_features,
            hidden_channels=self.num_features,
            num_layers=self.num_layers
        )


class GAT(GNN):
    def __init__(self, num_layers: int = 1, use_admittance: bool = True):
        super().__init__(num_layers, use_admittance)
        self.layer = pyg.nn.models.GAT(
            in_channels=self.num_features,
            hidden_channels=self.num_features,
            num_layers=self.num_layers
        )


class GIN(GNN):
    def __init__(self, num_layers: int = 1, use_admittance: bool = True):
        super().__init__(num_layers, use_admittance)
        self.layer = pyg.nn.models.GIN(
            in_channels=self.num_features,
            hidden_channels=self.num_features,
            num_layers=self.num_layers
        )


class GraphSAGE(GNN):
    def __init__(self, num_layers: int = 1, use_admittance: bool = True):
        super().__init__(num_layers, use_admittance)
        self.layer = pyg.nn.models.GraphSAGE(
            in_channels=self.num_features,
            hidden_channels=self.num_features,
            num_layers=self.num_layers
        )

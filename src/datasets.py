from functools import cached_property
import itertools
import numpy as np
import pandas as pd
import pandapower as pp
import torch
import torch_geometric as pyg
from energeai.topology import Topology
from src.ddlist import DeduplicatedList


class HomoDynamicDataset:
    def __init__(
            self,
            interpolation_fn: callable,
            topology: Topology,
            voltage: pd.DataFrame,
            norms: pd.DataFrame,
            states: DeduplicatedList
    ):
        self.topology = topology
        self.norms = norms
        self.Y = voltage.rename(columns=topology.reverse_mapping)
        self.Y = self.Y.loc[:, ~self.Y.columns.duplicated()]
        self.Y = self.Y.sort_index(axis=1)
        self.num_timesteps, self.num_nodes = self.Y.shape
        assert self.Y.notna().values.all()

        self.X = pd.concat([
            interpolation_fn(self.Y.iloc[rng.start:rng.stop],
                             topology.nxgraph, list(obs))
            for rng, obs in states.data.items()
        ])
        self.X.index, self.X.columns = self.Y.index, self.Y.columns

    @cached_property
    def pyg(self):
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        edge_index, edge_attr = self.topology.pyg
        input = np.dstack((self.X.values.real, self.X.values.imag))
        output = np.dstack((self.Y.values.real, self.Y.values.imag))
        return [pyg.data.Data(
            edge_index=edge_index,
            edge_attr=edge_attr,
            x=torch.tensor(x).float(),
            y=torch.tensor(y).float()
        ).to(device) for x, y in zip(input, output)]

    def check_voltage_band_violation(self, voltage):
        mu, sigma = self.norms[0], self.norms[1]
        mag = np.abs(voltage * sigma + mu)
        result = np.zeros_like(mag)
        result[np.where(mag > 0.9)] = -1
        result[np.where(mag < 1.1)] = 1
        return result


class StaticDataset(HomoDynamicDataset):
    def __init__(
            self,
            interpolation_fn: callable,
            topology: Topology,
            voltage: pd.DataFrame,
            norms: pd.DataFrame,
            observable_nodes: set[int],
    ):
        states = DeduplicatedList()
        states.insert(voltage.shape[0], observable_nodes)
        super().__init__(interpolation_fn, topology, voltage, norms, states)


class HeteroDynamicDataset:
    def __init__(
            self,
            interpolation_fn: callable,
            net: pp.pandapowerNet,
            voltage: pd.DataFrame,
            norms: pd.DataFrame,
            observable_nodes: set[int],
            switch_states: pd.DataFrame
    ):
        self.datasets = []

        # Determine when the topology changes
        changes = switch_states.shift() != switch_states
        changes = changes.fillna(True).aggregate(any, axis=1)
        switch_states = switch_states.loc[changes]
        switch_states = switch_states.transpose()

        # Create corresponding static datasets for each topology
        changes = switch_states.columns.append(voltage.index[-1:])
        for start, end in itertools.pairwise(changes):
            net.switch['closed'] = switch_states[start]
            topology = Topology(net)
            data = voltage[start:end]
            dataset = StaticDataset(
                interpolation_fn, topology, data, norms,
                {topology.reverse_mapping[n] for n in observable_nodes})
            self.datasets.append(dataset)

        self.check_voltage_band_violation = (
            self.datasets[0].check_voltage_band_violation)

    @cached_property
    def pyg(self):
        return list(itertools.chain.from_iterable(
            dataset.pyg for dataset in self.datasets
        ))

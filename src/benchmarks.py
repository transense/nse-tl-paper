import numpy as np
import torch
from energeai.topology import Topology
from energeai.interpolation import feature_propagation

from data.csv_loader import load_csv
from data.midas_cigre_mv.loader import load_cigre_mv
from src.datasets import StaticDataset, HomoDynamicDataset, HeteroDynamicDataset
from src.ddlist import DeduplicatedList
from src.models import GNN, GCN


def mad(y_pred, y_true):
    """Median absolute deviation visualizes better"""
    res = np.abs(y_pred - y_true)
    res = np.mean(res, axis=1)
    res = np.median(res)
    return res


def mse(y_pred, y_true):
    res = np.abs(y_pred - y_true) ** 2
    return res.mean()


def resilience(y_pred, y_true, condition):
    res = [np.mean(condition(y) == condition(z))
           for z, y in zip(y_pred, y_true)]
    return np.percentile(res, 5)


def evaluate(model, dataset, data):
    y_pred, y_true = model.predict(data)
    err = mse(y_pred, y_true)
    res = resilience(y_pred, y_true,
                     condition=dataset.check_voltage_band_violation)
    return err, res


def benchmarks(
    model_class: GNN,
    num_layers: int = 1,
    use_admittance: bool = True,
    interpolation_fn: callable = feature_propagation,
    ttsplit=0.5,
    observability=0.5,
    seed=42
):
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    rng = np.random.RandomState(seed)
    results = dict()

    # Load data for simbench_pq
    net_pq, voltage_pq, norms_pq, _ = load_csv("simbench_pq")
    num_timesteps_pq = len(voltage_pq.index)
    num_train_steps_pq = int(ttsplit * num_timesteps_pq)
    topology_pq = Topology(net_pq)
    num_nodes = topology_pq.num_nodes

    # Load data for simbench_tc
    net_tc, voltage_tc, norms, switch_states = load_csv("simbench_tc")

    # Load data for cigre-mv
    net_mv, voltage_mv, norms = load_cigre_mv()

    # Randomly initialize the first subset of observable nodes
    states = DeduplicatedList()
    topology_pq.vsensors = list(rng.choice(
        range(num_nodes), int(num_nodes * observability), replace=False))
    num_observable_nodes_0 = len(topology_pq.vsensors) - 1
    node_removal_interval, remainder = divmod(
        num_timesteps_pq - num_train_steps_pq, num_observable_nodes_0)
    states.insert(num_train_steps_pq, set(topology_pq.vsensors))

    # Generate observability degradation sequence
    for _ in range(num_observable_nodes_0):
        rng.shuffle(topology_pq.vsensors)
        topology_pq.vsensors.pop(-1)
        states.insert(node_removal_interval, set(topology_pq.vsensors))
    states.insert(remainder, states[-1])
    assert len(states) == num_timesteps_pq

    # Create the dataset for the baseline measurement
    dataset_pq = StaticDataset(
        interpolation_fn, topology_pq, voltage_pq, norms_pq, set(topology_pq.vsensors))
    pyg_pq = dataset_pq.pyg
    train_pq = pyg_pq[:int(len(pyg_pq) * ttsplit)]
    test_pq = pyg_pq[len(train_pq):]

    # Create the dataset for observability degradation use case
    dataset_od = HomoDynamicDataset(
        interpolation_fn, topology_pq, voltage_pq, norms_pq, states)
    pyg_od = dataset_od.pyg
    test_od = pyg_od[len(train_pq):]

    # Create the dataset for switching use case
    dataset_tc = HeteroDynamicDataset(
        interpolation_fn, net_tc, voltage_tc, norms,
        set(topology_pq.vsensors), switch_states
    )
    pyg_tc = dataset_tc.pyg
    train_tc = pyg_tc[:int(len(pyg_tc) * ttsplit)]
    test_tc = pyg_tc[len(train_tc):]

    # Create the dataset for CIGRE MV
    topology_mv = Topology(net_mv)
    num_nodes = topology_mv.num_nodes
    dataset_mv = StaticDataset(
        interpolation_fn, topology_mv, voltage_mv, norms,
        set(rng.choice(
            range(num_nodes), int(num_nodes * observability), replace=False))
    )
    pyg_mv = dataset_mv.pyg
    train_mv = pyg_mv[:int(len(pyg_mv) * ttsplit)]
    test_mv = pyg_mv[len(train_mv):]

    # Train on PQ data
    model1 = model_class(num_layers, use_admittance).to(device)
    model1.fit(train_pq)
    results["#params"] = model1.count_params()

    # Train on TC data
    model2 = model_class(num_layers, use_admittance).to(device)
    model2.fit(train_tc)

    # Train on CIGRE MV data
    model3 = model_class(num_layers, use_admittance).to(device)
    model3.fit(train_mv)

    # Run the baseline measurement
    results["train_base_mse"], results["train_base_res"] = (
        evaluate(model1, dataset_pq, train_pq))
    results["test_base_mse"], results["test_base_res"] = (
        evaluate(model1, dataset_pq, test_pq))

    # Run the observability degradation use case
    results["od_mse"], results["od_res"] = (
        evaluate(model1, dataset_od, test_od))

    # Run the switching use cases
    results["tc1_mse"], results["tc1_res"] = (
        evaluate(model1, dataset_tc, test_tc))
    results["tc2_mse"], results["tc2_res"] = (
        evaluate(model2, dataset_tc, test_tc))

    # Run the heterogeneous experiments
    results["pq2mv_mse"], results["pq2mv_res"] = (
        evaluate(model1, dataset_mv, test_mv))
    results["mv2pq_mse"], results["mv2pq_res"] = (
        evaluate(model3, dataset_mv, test_mv))

    return results


if __name__ == '__main__':
    benchmarks(GCN)

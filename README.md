This repository contains the source code and datasets for the paper

# On zero-shot learning in neural state estimation of power distribution systems

Before cloning this repository, install the [git-lfs plugin](https://git-lfs.com/): we are using LFS for the datasets.

To install, first get the appropriate version of PyTorch for your system configuration and then run `pip install -r requirements.txt`.

The file `gridsearch.py` contains the main experiment with the full grid search of the model parameter space. It produces the `results.csv` file that can subsequently be analyzed with `analysis.py`.

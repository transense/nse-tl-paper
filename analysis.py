import os

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

res_cols = ["test_base_res", "od_res", "tc1_res", "tc2_res", "pq2mv_res",
            "mv2pq_res"]
mse_cols = ["test_base_mse", "od_mse", "tc1_mse", "tc2_mse", "pq2mv_mse",
            "mv2pq_mse"]
bench_cols = res_cols + mse_cols

path = os.path.join(os.path.dirname(__file__), "results.csv")
try:
    results = pd.read_csv(path, header=0, index_col=False)
except FileNotFoundError:
    print("No results found to analyze, run experiments/gridsearch.py first")
    exit()

# A composite metric (res / mse) is used to rank models
results["test_base"] = results["test_base_res"] / results["test_base_mse"]
results["od"] = results["od_res"] / results["od_mse"]
results["tc1"] = results["tc1_res"] / results["tc1_mse"]
results["tc2"] = results["tc2_res"] / results["tc2_mse"]
results["pq2mv"] = results["pq2mv_res"] / results["pq2mv_mse"]
results["mv2pq"] = results["mv2pq_res"] / results["mv2pq_mse"]
composite_cols = ["test_base", "od", "tc1", "tc2", "pq2mv", "mv2pq"]

# Correlation analysis
plt.figure(figsize=(10, 10))
corr = results[["#params", "fp", "adm"] + composite_cols].corr()
heatmap = sns.heatmap(corr, vmin=-1, vmax=1, annot=True, cmap='coolwarm')
heatmap.set_title('Correlation Heatmap', fontdict={'fontsize': 12}, pad=12)

# Ranking tables
print("Best fit on pq data:")
print(results.sort_values(by="test_base", ascending=False)[
          ["model", "layers", "fp", "adm", "train_base_mse",
           "train_base_res", "test_base_mse", "test_base_res"]
      ].head(10).to_latex(index=False))
print("Best configurations for observability degradation:")
print(results.sort_values(by="od", ascending=False)[
          ["model", "layers", "fp", "adm", "od_mse", "od_res"]
      ].head(10).to_latex(index=False))
print("Best configurations for topology changes pq->tc:")
print(results.sort_values(by="tc1", ascending=False)[
          ["model", "layers", "fp", "adm", "tc1_mse", "tc1_res"]
      ].head(10).to_latex(index=False))
print("Best configurations for topology changes tc->tc:")
print(results.sort_values(by="tc2", ascending=False)[
          ["model", "layers", "fp", "adm", "tc2_mse", "tc2_res"]
      ].head(10).to_latex(index=False))
print("Best configurations for topology changes pq->mv:")
print(results.sort_values(by="pq2mv", ascending=False)[
          ["model", "layers", "fp", "adm", "pq2mv_mse", "pq2mv_res"]
      ].head(10).to_latex(index=False))
print("Best configurations for topology changes mv->pq:")
print(results.sort_values(by="mv2pq", ascending=False)[
          ["model", "layers", "fp", "adm", "mv2pq_mse", "mv2pq_res"]
      ].head(10).to_latex(index=False))

# Do params help?
plt.figure(figsize=(12, 6))
plt.xticks(results["#params"].unique())
for column in composite_cols:
    plt.scatter(results["#params"], results[column], label=column)
corr = results.groupby("#params")[composite_cols].mean().mean(axis=1)
plt.plot(corr.index, corr.values, "o-", label="Mean")
plt.xlabel("Number of parameters")
plt.ylabel("Resilience / MSE")
plt.yscale('log')
plt.legend()
plt.show()

# Do augmentations help?
diff = pd.concat([
    results[results['fp'] & results['adm']].mean(axis=0, numeric_only=True),
    results[results['fp'] & ~results['adm']].mean(axis=0, numeric_only=True),
    results[~results['fp'] & results['adm']].mean(axis=0, numeric_only=True),
    results[~results['fp'] & ~results['adm']].mean(axis=0, numeric_only=True)
], axis=1).T.astype({'fp': bool, 'adm': bool})
print(diff[['fp', 'adm'] + composite_cols].to_latex(index=False))

# Are the metrics aligned?
corrs = pd.Series()
for res, mse, col in zip(res_cols, mse_cols, composite_cols):
    corrs[col] = -np.corrcoef(results[res], results[mse])[0, 1]
print(corrs.to_string())
